Explanation
===========
At the top [of the file](https://bitbucket.org/joshuacottrell/fun-comparison02/master/src/src/functional-comparison.js) there are two shared constants for the tests at the
bottom.

The [es2015 arrow function notation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions) is used for functions. All functions are
pure except the `console.log` calls.

I named it 'straight-on' instead of imperitive because `soIsNumber` is used
functionally instead of replacing each call with something like
`typeof percentage === 'number' && !isNaN(percentage)`. That seemed
unnecessarily biased :angel:.

It should be noted that this is a sample and the more re-usable functions are
used, the more the functional style surpasses the straight-on.

Once liftA3 is a known quantity understanding the functional approach is the
same as the straight-on js approach except that the functions are reusable and
more expressive. There is also the possibility of more productivity because
the next time you need to check for the validity of a number you don't have to
think about `typeof` and `NaN` cases.

Use
===
Steps:  
    λ > npm install  
    λ > npm run build && node dist/functional-comparison.js
