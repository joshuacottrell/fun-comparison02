/*eslint-disable no-console*/
const targeted = 75
const duration = 1646

/* Straight-on JS */
const soIsNumber = x => typeof x === 'number' && !isNaN(x)

const isSoPercentageOfInterest = (percentage, total, current) => {
  if (
    soIsNumber(percentage) &&
    soIsNumber(total) &&
    (total !== 0) &&
    soIsNumber(current)
  ) {
    return (((current / total) * 100)) > percentage
  }
  return false
}

/* functional JS */
import {curry} from 'ramda'
import safe from 'crocks/Maybe/safe'
import isNumber from 'crocks/predicates/isNumber'
import liftA3 from 'crocks/helpers/liftA3'

const safeNumber = safe(isNumber)
const safeDividend = safe(n => n !== 0)
const unsafeIsPercentageOfInterest = curry((percent, total, current) =>
  (((current / total) * 100)) > percent)


const isFunPercentageOfInterest = curry((percent, total, current) =>
  liftA3(
    unsafeIsPercentageOfInterest,
    safeNumber(percent),
    safeNumber(total).chain(safeDividend),
    safeNumber(current)
  ).option(false))

console.log('test s-o low', isSoPercentageOfInterest(targeted, duration, 170))
console.log('test s-o high', isSoPercentageOfInterest(targeted, duration, 1235))
console.log('test fun low', isFunPercentageOfInterest(targeted, duration, 170))
console.log('test fun high',isFunPercentageOfInterest(targeted, duration, 1235))
